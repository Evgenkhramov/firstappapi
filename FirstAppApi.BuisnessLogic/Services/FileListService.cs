﻿using AutoMapper;
using FirstAppApi.BuisnessLogic.Interfaces;
using FirstAppApi.DataAccess.Entities;
using FirstAppApi.DataAccess.Interfaces;
using FirstAppApi.Models.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FirstAppApi.BuisnessLogic.Services
{
    public class FileListService : IFileListService
    {
        #region Variables

        private readonly IMapper _mapper;
        private readonly IFileRepository _fileRepository;

        #endregion Variables

        #region Constructors

        public FileListService(IFileRepository fileRepository, IMapper mapper)
        {
            _fileRepository = fileRepository;
            _mapper = mapper;
        }

        public async Task<bool> DeleteFiles(int taskId)
        {
            bool result = await _fileRepository.DeleteFiles(taskId);

            return result;
        }

        public async Task<List<FileModel>> GetListByTaskId(int taskId)
        {
            List<FileListEntity> result = await _fileRepository.GetListByTaskId(taskId);
           
            if (result == null || result.Count == 0)
            {
                return null;
            }

            List<FileModel> mapperResult = _mapper.Map<List<FileListEntity>, List<FileModel>>(result);

            return mapperResult;
        }

        public async Task<bool> InsertTaskList(List<FileListEntity> list)
        {
            bool result = await _fileRepository.InsertRange(list);

            return result;
        }

        #endregion Constructors
    }
}
