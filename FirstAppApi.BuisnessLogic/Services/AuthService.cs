﻿using FirstAppApi.BuisnessLogic.Auth;
using FirstAppApi.BuisnessLogic.Interfaces;
using FirstAppApi.LocalizationLibrary;
using FirstAppApi.Models;
using FirstAppApi.Models.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace FirstAppApi.BuisnessLogic.Services
{
    public class AuthService : IAuthService
    {
        #region Variables

        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;

        #endregion Variables

        #region Constructors

        public AuthService(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
        }

        #endregion Constructors

        #region Methods

        public async Task<IdentityResult> InsertUser(RegistrationViewModel user)
        {
            AppUser thisUser = new AppUser() { };

            thisUser.Email = user.Email;
            thisUser.UserName = user.Email;

            IdentityResult resultIdenty = await _userManager.CreateAsync(thisUser, user.Password);

            return resultIdenty;
        }

        public async Task<ApiLoginResponse> Login(LoginViewModel user)
        {
            AppUser findUser = await _userManager.FindByEmailAsync(user.Email);

            if (findUser == null || string.IsNullOrEmpty(findUser.Id))
            {
                var loginResult = new ApiLoginResponse();
                loginResult.Error = ApplicationStrings.ThereIsNoUserWithSuchEmail;
                loginResult.Result = System.Net.HttpStatusCode.NotFound;

                return loginResult;
            }

            SignInResult checkPasswordResult = await _signInManager.PasswordSignInAsync(findUser, user.Password, user.RememberMe, false);

            if (!checkPasswordResult.Succeeded)
            {
                var loginResult = new ApiLoginResponse();
                loginResult.Error = ApplicationStrings.PasswordIsIncorrect;
                loginResult.Result = System.Net.HttpStatusCode.BadRequest;

                return loginResult;
            }

            var clime = new Claim(ClaimTypes.Name, user.Email);
            await _userManager.AddClaimAsync(findUser, clime);
            IEnumerable<Claim> identity = await GetIdentity(findUser);
            string token = CreateJWT(identity);
            string refreshToken = GenerateRefreshToken();
            findUser.RefreshToken = refreshToken;
            await _userManager.UpdateAsync(findUser);

            var result = new ApiLoginResponse();
            result.Token = token;
            result.RefreshToken = refreshToken;
            result.Result = System.Net.HttpStatusCode.OK;
            result.UserId = findUser.Id;

            return result;
        }

        public async Task<bool> Logout()
        {
            await _signInManager.SignOutAsync();

            return true;
        }

        private async Task<IEnumerable<Claim>> GetIdentity(AppUser user)
        {
            if (user == null)
            {
                return null;
            }

            var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Id)
                };

            return claims;
        }

        private string CreateJWT(IEnumerable<Claim> claims)
        {
            var now = DateTime.UtcNow;
            // создаем JWT-токен
            var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    audience: AuthOptions.AUDIENCE,
                    notBefore: now,
                    claims: claims,
                    expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return encodedJwt;
        }

        private string GenerateRefreshToken()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);

                return Convert.ToBase64String(randomNumber);
            }
        }

        public async Task<ApiLoginResponse> RefreshToken(ApiLoginResponse user)
        {
            var principal = GetPrincipalFromExpiredToken(user.Token);
            var userId = principal.Identity.Name;
            AppUser findUser = await _userManager.FindByIdAsync(userId);
            var savedRefreshToken = findUser.RefreshToken;
            if (savedRefreshToken != user.RefreshToken)
            {
                var answer = new ApiLoginResponse() { Error = ApplicationStrings.InvalidRefreshToken, Result = System.Net.HttpStatusCode.Unauthorized };

                return answer;
                //throw new SecurityTokenException("Invalid refresh token");
            }

            var newJwtToken = CreateJWT(principal.Claims);
            var newRefreshToken = GenerateRefreshToken();
            findUser.RefreshToken = newRefreshToken;

            var result = new ApiLoginResponse()
            {
                Result = System.Net.HttpStatusCode.OK,
                RefreshToken = newRefreshToken,
                Token = newJwtToken,
                UserId = user.UserId
            };

            return result;
        }

        private ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false,
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                ValidateLifetime = false //here we are saying that we don't care about the token's expiration date
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            ClaimsPrincipal principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException(ApplicationStrings.InvalidToken);

            return principal;
        }

        #endregion Methods
    }
}
