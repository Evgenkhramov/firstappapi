﻿using AutoMapper;
using FirstAppApi.BuisnessLogic.Interfaces;
using FirstAppApi.DataAccess.Entities;
using FirstAppApi.DataAccess.Interfaces;
using FirstAppApi.Models.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstAppApi.BuisnessLogic.Services
{
    public class MapMarkerService : IMapMarkerService
    {
        #region Variables       

        private readonly IMapper _mapper;
        private readonly IMarkersRepository _markersRepository;

        #endregion Variables

        #region Constructors

        public MapMarkerService(IMarkersRepository markersRepository, IMapper mapper)
        {
            _markersRepository = markersRepository;
            _mapper = mapper;
        }

        #endregion Constructors
        public async Task<bool> DeleteMapMarker(MapMarkerEntity marker)
        {
            bool result = await _markersRepository.Delete(marker);

            return result;
        }

        public async Task<bool> DeleteMapMarkerRange(List<MapMarkerEntity> markerList)
        {
            bool result = await _markersRepository.DeleteRange(markerList);

            return result;
        }

        public async Task<bool> DeleteMarker(int markerId)
        {
            bool result = await _markersRepository.Delete(markerId);

            return result;
        }

        public async Task<bool> DeleteMarkers(int taskId)
        {
            bool result = await _markersRepository.DeleteMarkers(taskId);

            return result;
        }

        public async Task<List<MapMarkerModel>> GetAll()
        {
            List<MapMarkerEntity>  result = await _markersRepository.GetAll();
            if (result == null || !result.Any())
            {
                return null;
            }

            List<MapMarkerModel> mapperResult = _mapper.Map<List<MapMarkerEntity>, List<MapMarkerModel>>(result);

            return mapperResult;
        }

        public async Task<List<MapMarkerModel>> GetMarkersList(int taskId)
        {
            List<MapMarkerEntity> result = await _markersRepository.GetMarkersList(taskId);

            if (result == null || !result.Any())
            {
                return null;
            }

            List<MapMarkerModel> mapperResult = _mapper.Map<List<MapMarkerEntity>, List<MapMarkerModel>>(result);

            return mapperResult;
        }

        public async Task<bool> InsertMapMarkerRange(List<MapMarkerModel> markerList)
        {
            if (markerList == null || !markerList.Any())
            {
                return false;
            }

            List<MapMarkerEntity> mapperResult = _mapper.Map<List<MapMarkerModel>, List<MapMarkerEntity>>(markerList);

            bool result = await _markersRepository.InsertRange(mapperResult);

            return result;
        }
    }
}
