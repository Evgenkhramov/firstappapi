﻿using FirstAppApi.BuisnessLogic.Interfaces;
using FirstAppApi.DataAccess.Entities;
using FirstAppApi.DataAccess.Interfaces;
using FirstAppApi.Models;
using FirstAppApi.Models.Models;
using Microsoft.AspNetCore.Identity;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace FirstAppApi.BuisnessLogic.Services
{
    public class UserService : IUserService
    {
        #region Variables

        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly IUserRepository _userRepository;

        #endregion Variables

        #region Constructors

        public UserService(IUserRepository userRepository, UserManager<AppUser> userManager, SignInManager<AppUser> signInManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _userRepository = userRepository;
        }

        #endregion Constructors

        #region Methods
        public async Task DeleteUser(int userID)
        {
            await _userRepository.Delete(userID);
        }

        public async Task<bool> UserAuthorization(LoginViewModel user)
        {

            bool isEmailInDB = await _userRepository.CheckEmailInDB(user.Email);
            if (!isEmailInDB)
            {
                return false;
            }

            UserEntity thisUserInDB = await _userRepository.GetUserByEmail(user.Email);

            SHA256 sha256 = SHA256.Create();
            byte[] userPasswordByte = sha256.ComputeHash(Encoding.UTF8.GetBytes(user.Password));
            string userPassword = Encoding.UTF8.GetString(userPasswordByte);


            if (thisUserInDB.Password != userPassword)
            {
                return false;
            }

            return true;
        }

        public async Task<UserEntity> GetUserByEmail(string email)
        {
            UserEntity user = await _userRepository.GetUserByEmail(email);

            return user;
        }

        public async Task<UserEntity> GetUserById(int userId)
        {
            UserEntity user = await _userRepository.FindById(userId);

            return user;
        }

        public async Task<bool> CheckEmailInDB(string email)
        {
            bool result = await _userRepository.CheckEmailInDB(email);

            return result;
        }

        #endregion Methods
    }
}
