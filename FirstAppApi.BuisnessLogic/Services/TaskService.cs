﻿using AutoMapper;
using FirstAppApi.BuisnessLogic.Interfaces;
using FirstAppApi.DataAccess.Entities;
using FirstAppApi.DataAccess.Interfaces;
using FirstAppApi.Models.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstAppApi.BuisnessLogic.Services
{
    public class TaskService : ITaskService
    {
        #region Variables

        private readonly IMapper _mapper;
        private readonly ITaskRepository _taskRepository;

        #endregion Variables

        #region Constructors

        public TaskService(ITaskRepository taskRepository, IMapper mapper)
        {
            _mapper = mapper;
            _taskRepository = taskRepository;
        }

        #endregion Constructors

        #region Methods

        public async Task<TaskListViewModel> GetAllUserTaskList(int userId)
        {
            List<TaskEntity> taskList = await _taskRepository.GetUserTasks(userId);
            if (!taskList.Any())
            {
                return null;
            }

            List<TaskMainTaskListItem> resultList = Mapper.Map<List<TaskEntity>, List<TaskMainTaskListItem>>(taskList);

            var result = new TaskListViewModel();
            result.TaskList = resultList;

            return result;
        }

        public async Task<int> InsertTask(TaskViewModel task)
        {
            TaskEntity _mapperTaskResult = _mapper.Map<TaskViewModel, TaskEntity>(task);
            await _taskRepository.Insert(_mapperTaskResult);
            int taskId = _mapperTaskResult.Id;

            return taskId;
        }

        public async Task<bool> DeleteTask(TaskViewModel task)
        {
            TaskEntity _mapperResult = _mapper.Map<TaskViewModel, TaskEntity>(task);
            bool result = await _taskRepository.Delete(_mapperResult);

            return result;
        }

        public async Task<bool> UpdateTask(TaskViewModel task)
        {
            TaskEntity _mapperResult = _mapper.Map<TaskViewModel, TaskEntity>(task);
            bool result = await _taskRepository.Update(_mapperResult);

            return result;
        }

        public async Task<List<TaskViewModel>> GetTask(int userId)
        {
            List<TaskEntity> result = await _taskRepository.GetUserTasks(userId);

            if (result == null || result.Count == 0)
            {
                return null;
            }
            List<TaskViewModel> taskList = _mapper.Map<List<TaskEntity>, List<TaskViewModel>>(result);

            return taskList;
        }

        public async Task<List<TaskViewModel>> GetAllTask()
        {
            List<TaskEntity> result = await _taskRepository.GetAll();

            if (result == null || result.Count == 0)
            {
                return null;
            }
            List<TaskViewModel> taskList = _mapper.Map<List<TaskEntity>, List<TaskViewModel>>(result);

            return taskList;
        }

        #endregion Methods
    }
}
