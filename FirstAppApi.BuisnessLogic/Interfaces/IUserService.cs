﻿using FirstAppApi.DataAccess.Entities;
using FirstAppApi.Models;
using System.Threading.Tasks;

namespace FirstAppApi.BuisnessLogic.Interfaces
{
    public interface IUserService
    {
        Task<bool> UserAuthorization(LoginViewModel user);
        Task<UserEntity> GetUserByEmail(string email);
        Task<UserEntity> GetUserById(int userId);
        Task DeleteUser(int userID);
        Task<bool> CheckEmailInDB(string email);
    }
}
