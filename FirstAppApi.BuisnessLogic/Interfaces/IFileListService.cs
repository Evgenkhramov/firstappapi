﻿using FirstAppApi.DataAccess.Entities;
using FirstAppApi.Models.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FirstAppApi.BuisnessLogic.Interfaces
{
    public interface IFileListService
    {
        Task<bool> InsertTaskList(List<FileListEntity> list);

        Task<List<FileModel>> GetListByTaskId(int taskId);

        Task<bool> DeleteFiles(int taskId);
    }
}
