﻿using FirstAppApi.DataAccess.Entities;
using FirstAppApi.Models.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FirstAppApi.BuisnessLogic.Interfaces
{
    public interface IMapMarkerService
    {
        Task<bool> InsertMapMarkerRange(List<MapMarkerModel> markerList);

        Task<List<MapMarkerModel>> GetMarkersList(int taskId);

        Task<List<MapMarkerModel>> GetAll();

        Task<bool> DeleteMapMarker(MapMarkerEntity marker);

        Task<bool> DeleteMapMarkerRange(List<MapMarkerEntity> markerList);

        Task<bool> DeleteMarker(int id);

        Task<bool> DeleteMarkers(int taskId);

    }
}
