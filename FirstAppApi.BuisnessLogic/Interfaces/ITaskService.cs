﻿using FirstAppApi.Models.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FirstAppApi.BuisnessLogic.Interfaces
{
    public interface ITaskService
    {
        Task<TaskListViewModel> GetAllUserTaskList(int userId);

        Task<int> InsertTask(TaskViewModel task);

        Task<bool> DeleteTask(TaskViewModel task);

        Task<bool> UpdateTask(TaskViewModel task);

        Task<List<TaskViewModel>> GetTask(int userId);

        Task<List<TaskViewModel>> GetAllTask();
    }
}
