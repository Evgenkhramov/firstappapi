﻿using FirstAppApi.Models;
using FirstAppApi.Models.Models;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace FirstAppApi.BuisnessLogic.Interfaces
{
    public interface IAuthService
    {
        Task<bool> Logout();

        Task<IdentityResult> InsertUser(RegistrationViewModel user);

        Task<ApiLoginResponse> Login(LoginViewModel user);

        Task<ApiLoginResponse> RefreshToken(ApiLoginResponse user);
    }
}
