﻿using AutoMapper;
using FirstAppApi.DataAccess.Entities;
using FirstAppApi.Models.Models;
using FirstAppApi.Models.ViewModels;

namespace FirstAppApi.BuisnessLogic.MapperProfiles
{
    public class MapperProfile : Profile
    {
        #region Constructors

        public MapperProfile()
        {
            CreateMap<TaskEntity, TaskMainTaskListItem>()
                .ForMember(dest => dest.TaskID, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.TaskName, opt => opt.MapFrom(src => src.TaskName))
                .ForMember(dest => dest.TaskDeskription, opt => opt.MapFrom(src => src.TaskDescription));

            CreateMap<TaskViewModel, TaskEntity>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.TaskName, opt => opt.MapFrom(src => src.TaskName))
                .ForMember(dest => dest.TaskDescription, opt => opt.MapFrom(src => src.TaskDescription));

            CreateMap<TaskEntity, TaskViewModel>()
               .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
               .ForMember(dest => dest.TaskName, opt => opt.MapFrom(src => src.TaskName))
               .ForMember(dest => dest.TaskDescription, opt => opt.MapFrom(src => src.TaskDescription));

            CreateMap<MapMarkerModel, MapMarkerEntity>()
                .ForMember(dest => dest.TaskId, opt => opt.MapFrom(src => src.TaskId))
                .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.Latitude))
                .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.Longitude));

            CreateMap<FileModel, FileListEntity>()
                .ForMember(dest => dest.TaskId, opt => opt.MapFrom(src => src.TaskId))
                .ForMember(dest => dest.FileName, opt => opt.MapFrom(src => src.FileName));

            CreateMap<FileListEntity, FileModel>()
                .ForMember(dest => dest.TaskId, opt => opt.MapFrom(src => src.TaskId))
                .ForMember(dest => dest.FileName, opt => opt.MapFrom(src => src.FileName));
        }

        #endregion Constructors
    }
}
