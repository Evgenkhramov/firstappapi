﻿using AutoMapper;
using FirstAppApi.BuisnessLogic.Interfaces;
using FirstAppApi.BuisnessLogic.MapperProfiles;
using FirstAppApi.BuisnessLogic.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Diagnostics;

namespace FirstAppApi.BuisnessLogic
{
    public class Startup
    {
        public Startup()
        {
        }

        public static void Init(IServiceCollection services)
        {
            ConfigureAutoMapper(services);

            RegistrationServices(services);

            DataAccess.Startup.Init(services);
        }

        public static void ConfigureAutoMapper(IServiceCollection services)
        {
            var mapperConfiguration = new MapperConfiguration(config =>
            {
                config.AddProfile(new MapperProfile());
            });

            IMapper mapper = mapperConfiguration.CreateMapper();

            services.AddSingleton(mapper);
        }

        public static void RegistrationServices(IServiceCollection services)
        {
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ITaskService, TaskService>();
            services.AddScoped<IMapMarkerService, MapMarkerService>();
        }
    }
}
