﻿using FirstAppApi.BuisnessLogic.Interfaces;
using FirstAppApi.Models.ViewModels;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Nancy.Json;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FirstAppApi.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserService _userService;
        private readonly ITaskService _taskService;

        public HomeController(IUserService userService,ITaskService taskService)
        {
            _userService = userService;
            _taskService = taskService;
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public IActionResult Index()
        {
            ViewBag.Name = User.Identity.Name;

            return View();
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public IActionResult GetLogin()
        {
            Ok($"Ваш логин: {User.Identity.Name}");

            return View();
        }

        [HttpPost]
        [Route("home/index")]
        public JsonResult GetAllTasks(int userId)
        {
            TaskListViewModel result = _taskService.GetAllUserTaskList(userId).Result;

             //jsonResult = JsonConvert.SerializeObject(result);

            return Json(result);
        }


    }
}
