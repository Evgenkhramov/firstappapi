﻿using FirstAppApi.BuisnessLogic.Interfaces;
using FirstAppApi.Models.ViewModels;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FirstAppApi.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class HomeApiController : ControllerBase
    {

        #region Variables

        private readonly IMapMarkerService _mapMarkerService;
        private readonly ITaskService _taskService;
        private readonly IFileListService _fileListService;

        #endregion Variables

        public HomeApiController(IMapMarkerService mapMarkerService, ITaskService taskService, IFileListService fileListService)
        {
            _fileListService = fileListService;
            _taskService = taskService;
            _mapMarkerService = mapMarkerService;
        }

        [HttpGet]
        [Route("GetUser/{userId}")]
        public async Task<TaskListViewModel> GetTasks(int userId)
        {
            TaskListViewModel result = await _taskService.GetAllUserTaskList(userId);

            return result;
        }
    }
}
