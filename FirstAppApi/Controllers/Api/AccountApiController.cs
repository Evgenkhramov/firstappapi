﻿using FirstAppApi.BuisnessLogic.Interfaces;
using FirstAppApi.Models;
using FirstAppApi.Models.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace FirstAppApi.Controllers.Api
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountApiController : ControllerBase
    {
        private readonly IAuthService _authService;

        public AccountApiController(IAuthService authService)
        {
            _authService = authService;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("Registration")]
        public async Task<ApiLoginResponse> Registration([FromBody]RegistrationViewModel user)
        {
            IdentityResult result = await _authService.InsertUser(user);

            if (result.Succeeded)
            {
                var answer = new ApiLoginResponse() { Result = HttpStatusCode.OK };

                return answer;
            }

            var answerError = new ApiLoginResponse() { Result = HttpStatusCode.BadRequest, Error = $"{result.Errors.FirstOrDefault().Code.ToString()}, {result.Errors.FirstOrDefault().Description.ToString()}" };

            return answerError;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("Login")]
        public async Task<ApiLoginResponse> Login([FromBody] LoginViewModel user)
        {
            ApiLoginResponse loginResult = await _authService.Login(user);

            return loginResult;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost]
        [Route("Logout")]
        public async Task<bool> Logout()
        {
            bool answer = await _authService.Logout();

            return answer;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost]
        [Route("Logout")]
        public async Task<bool> GetUserData()
        {
            bool answer = await _authService.Logout();

            return answer;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("Refresh")]
        public async Task<ApiLoginResponse> Refresh([FromBody]ApiLoginResponse user)
        {
            ApiLoginResponse result = await _authService.RefreshToken(user);

            return result;
        }
    }
}
