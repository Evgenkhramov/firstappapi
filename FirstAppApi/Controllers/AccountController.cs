﻿using FirstAppApi.BuisnessLogic.Interfaces;
using FirstAppApi.LocalizationLibrary;
using FirstAppApi.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FirstAppApi.Controllers
{
    public class AccountController : Controller
    {
        #region Variables

        private readonly IUserService _userService;
        private readonly IAuthService _authService;

        #endregion Variables

        #region Constructors

        public AccountController(IUserService userService, IAuthService authService)
        {
            _authService = authService;
            _userService = userService;
        }

        #endregion Constructors

        #region Methods

        [HttpGet]
        public IActionResult Registration()
        {
            RegistrationViewModel view = new RegistrationViewModel() { };

            return View(view);
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            LoginViewModel model = new LoginViewModel();

            model.ReturnUrl = returnUrl;

            return View(model);
        }

        [HttpGet]
        [ResponseCache(Duration = default(int), Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel user)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", ApplicationStrings.EnteAllData);

                return View(user);
            }

            var answer = await _authService.Login(user);

            if (answer.Result == HttpStatusCode.OK)
            {
                await Authenticate(user.Email);

                return RedirectToAction("Index", "Home");
            }

            ModelState.AddModelError("", ApplicationStrings.UserEmailOrPasswordInvalid);

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Registration(RegistrationViewModel user)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", ApplicationStrings.UserEmailOrPasswordInvalid);

                return View(user);
            }

            Microsoft.AspNetCore.Identity.IdentityResult registrationResult = await _authService.InsertUser(user);

            if (!registrationResult.Succeeded)
            {
                ModelState.AddModelError("", ApplicationStrings.UseDidNotRegister);

                return View(user);
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public async Task<IActionResult> LogOut()
        {
            await _authService.Logout();

            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult UserProfile()
        {
            UserProfileViewModel view = new UserProfileViewModel() { };

            return View(view);
        }

        private async Task Authenticate(string email)
        {
            // создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, email)
            };
            // создаем объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            // установка аутентификационных куки
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        #endregion Methods
    }
}
