﻿using Microsoft.AspNetCore.Identity;

namespace FirstAppApi.Models.Models
{
    public class AppUser : IdentityUser
    {
        public string RefreshToken { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserPhoto { get; set; }
    }
}

