﻿namespace FirstAppApi.Models.Models
{
    public class FileModel
    {
        public int TaskId { get; set; }
        public string FileName { get; set; }
    }
}
