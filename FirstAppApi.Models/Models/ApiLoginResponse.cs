﻿using System.Net;

namespace FirstAppApi.Models.Models
{
    public class ApiLoginResponse
    {
        public HttpStatusCode Result { get; set; } 
        public string UserId { get; set; }
        public string Error { get; set; }
        public string Token{ get; set; }
        public string RefreshToken{ get; set; }
    }
}
