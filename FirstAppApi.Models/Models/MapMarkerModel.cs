﻿namespace FirstAppApi.Models.Models
{
    public class MapMarkerModel
    {
        public int TaskId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
