﻿using System.ComponentModel.DataAnnotations;

namespace FirstAppApi.Models
{
    public class UserProfileViewModel
    {
        public string UserName { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}
