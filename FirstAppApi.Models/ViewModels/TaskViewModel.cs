﻿using FirstAppApi.Models.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FirstAppApi.Models.ViewModels
{
    public class TaskViewModel
    {
        public string Id { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string UserId { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string TaskName { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string TaskDescription { get; set; }

        public List<MapMarkerModel> MapMarkers { get; set; }

        public List<FileModel> FileModels { get; set; }
    }
}
