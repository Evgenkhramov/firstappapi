﻿using System.Collections.Generic;

namespace FirstAppApi.Models.ViewModels
{
    public class TaskListViewModel
    {
        public List<TaskMainTaskListItem> TaskList { get; set; }

        public TaskListViewModel()
        {
            TaskList = new List<TaskMainTaskListItem>();
        }
    }

    public class TaskMainTaskListItem
    {
        public string TaskID { get; set; }

        public string TaskName { get; set; }

        public string TaskDeskription { get; set; }
    }
}
