﻿using FirstAppApi.DataAccess.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FirstAppApi.DataAccess.Interfaces
{
    public interface IMarkersRepository : IBaseRepository<MapMarkerEntity>
    {
        Task<List<MapMarkerEntity>> GetMarkersList(int taskId);
        Task<bool> DeleteMarkers(int taskId);
    }
}
