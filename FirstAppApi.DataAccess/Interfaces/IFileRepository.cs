﻿using FirstAppApi.DataAccess.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FirstAppApi.DataAccess.Interfaces
{
    public interface IFileRepository : IBaseRepository<FileListEntity>
    {
        Task<List<FileListEntity>> GetListByTaskId(int taskId);

        Task<bool> DeleteFiles(int taskId);
    }
}
