﻿using FirstAppApi.DataAccess.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FirstAppApi.DataAccess.Interfaces
{
    public interface ITaskRepository : IBaseRepository<TaskEntity>
    {
        Task<List<TaskEntity>> GetUserTasks(int userId);
    }
}
