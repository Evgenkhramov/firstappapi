﻿using FirstAppApi.DataAccess.Entities;
using System.Threading.Tasks;

namespace FirstAppApi.DataAccess.Interfaces
{
    public interface IUserRepository : IBaseRepository<UserEntity>
    {
        Task<UserEntity> GetUserByEmail(string email);
        Task<int> GetUserIdByEmail(string email);
        Task<bool> CheckEmailInDB(string email);
    }
}
