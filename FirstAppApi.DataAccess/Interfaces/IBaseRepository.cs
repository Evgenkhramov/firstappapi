﻿using FirstAppApi.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FirstAppApi.DataAccess.Interfaces
{
    public interface IBaseRepository<T> : IDisposable where T : IBaseEntity
    {
        Task<int> GetCount();
        Task<T> FindById(int id);
        Task<List<T>> GetAll();
        Task<bool> Insert(T entity);
        Task<bool> InsertRange(List<T> list);
        Task<bool> Update(T entity);
        Task<bool> UpdateRange(List<T> list);
        Task<bool> Delete(T entity);
        Task<bool> Delete(int itemId);
        Task<bool> DeleteRange(List<T> list);
    }
}
