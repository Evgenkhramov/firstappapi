﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FirstAppApi.Data.Migrations
{
    public partial class toolfix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserId",
                table: "UserEntitys");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "TaskEntitys",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "UserId",
                table: "UserEntitys",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "TaskEntitys",
                nullable: true,
                oldClrType: typeof(int));
        }
    }
}
