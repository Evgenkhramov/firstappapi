﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FirstAppApi.Data.Migrations
{
    public partial class ChangeUserEntityPasswordTypeToString : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Password",
                table: "UserEntitys",
                nullable: true,
                oldClrType: typeof(byte[]),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<byte[]>(
                name: "Password",
                table: "UserEntitys",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
