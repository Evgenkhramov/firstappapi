﻿using FirstAppApi.DataAccess.Entities;
using FirstAppApi.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstAppApi.DataAccess.Repositories
{
    public class FileEFRepository : BaseEFRepository<FileListEntity>, IFileRepository
    {
        #region Constructors
        public FileEFRepository(ApplicationDbContext context) : base(context)
        {

        }

        #endregion Constructors

        #region Methods

        public async Task<List<FileListEntity>> GetListByTaskId(int taskId)
        {
            List<FileListEntity> list = await _dbSet.Where(x => x.TaskId == taskId).ToListAsync();

            return list;
        }

        public async Task<bool> DeleteFiles(int taskId)
        {
            List<FileListEntity> itemList = await _dbSet.Where(x => x.TaskId == taskId).ToListAsync();

            _dbSet.RemoveRange(itemList);

            int taskResult = await _context.SaveChangesAsync();

            return taskResult != default(int);
        }

        #endregion Methods
    }
}
