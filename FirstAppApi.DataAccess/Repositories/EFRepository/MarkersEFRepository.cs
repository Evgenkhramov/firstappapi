﻿using FirstAppApi.DataAccess.Entities;
using FirstAppApi.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstAppApi.DataAccess.Repositories
{
    public class MarkersEFRepository : BaseEFRepository<MapMarkerEntity>, IMarkersRepository
    {
        #region Constructors
        public MarkersEFRepository(ApplicationDbContext context) : base(context)
        {
        }

        #endregion Constructors

        #region Methods

        public async Task<List<MapMarkerEntity>> GetMarkersList(int taskId)
        {
            List<MapMarkerEntity> list = await _dbSet.Where(x => x.TaskId == taskId).ToListAsync();

            return list;
        }

        public async Task<bool> DeleteMarkers(int taskId)
        {
            var itemList = await _dbSet.Where(x => x.TaskId == taskId).ToListAsync();

            _dbSet.RemoveRange(itemList);

            int taskResult = await _context.SaveChangesAsync();

            return taskResult != default(int);
        }

        #endregion Methods
    }
}
