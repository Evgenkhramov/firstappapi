﻿using FirstAppApi.DataAccess.Entities;
using FirstAppApi.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstAppApi.DataAccess.Repositories
{
    class TaskEFRepository : BaseEFRepository<TaskEntity>, ITaskRepository
    {
        #region Constructors

        public TaskEFRepository(ApplicationDbContext context) : base(context)
        {
        }

        #endregion Constructors

        #region Methods

        public async Task<List<TaskEntity>> GetUserTasks(int userId)
        {
            List<TaskEntity> taskList = await _dbSet.Where(x => x.UserId == userId).ToListAsync();

            return taskList;
        }

        #endregion Methods
    }
}
