﻿using FirstAppApi.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstAppApi.DataAccess.Repositories
{
    public class BaseEFRepository<T> : IBaseRepository<T> where T : class, IBaseEntity
    {
        #region Variables

        protected readonly DbSet<T> _dbSet;

        protected readonly ApplicationDbContext _context;

        #endregion Variables

        #region Constructors

        public BaseEFRepository(ApplicationDbContext context)
        {
            _context = context;

            _dbSet = context.Set<T>();
        }

        #endregion Constructors

        #region Methods
        public async Task<int> GetCount()
        {
            int count = await _dbSet.CountAsync<T>();

            return count;
        }

        public async Task<T> FindById(int id)
        {
            T item = await _dbSet.FirstOrDefaultAsync(i => i.Id == id);

            return item;
        }

        public async Task<List<T>> GetAll()
        {
            List<T> list = await _dbSet.ToListAsync();

            return list;
        }

        public async Task<bool> Insert(T entity)
        {
            await _dbSet.AddAsync(entity);

            var answ = await _context.SaveChangesAsync();

            return answ != default(int);
        }

        public async Task<bool> InsertRange(List<T> list)
        {
            await _dbSet.AddRangeAsync(list);

            int taskResult = await _context.SaveChangesAsync();

            return taskResult == list.Count;
        }

        public async Task<bool> Update(T entity)
        {
            _dbSet.Update(entity);

            int taskResult = await _context.SaveChangesAsync();

            return taskResult != default(int);
        }

        public async Task<bool> UpdateRange(List<T> list)
        {
            _dbSet.UpdateRange(list);

            int taskResult = await _context.SaveChangesAsync();

            return taskResult == list.Count;
        }

        public async Task<bool> Delete(T entity)
        {
            _dbSet.Remove(entity);

            int taskResult = await _context.SaveChangesAsync();

            return taskResult != default(int);
        }

        public async Task<bool> Delete(int id)
        {
            T item = _dbSet.FirstOrDefault(x => x.Id == id);

            _dbSet.Remove(item);

            int taskResult = await _context.SaveChangesAsync();

            return taskResult != default(int);
        }
        public async Task<bool> DeleteRange(List<T> list)
        {
            _dbSet.RemoveRange(list);

            int taskResult = await _context.SaveChangesAsync();

            return taskResult==list.Count;
        }

        public void Dispose()
        {
            _context.Dispose();
        }
        #endregion Methods
    }
}
