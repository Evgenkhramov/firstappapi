﻿using FirstAppApi.DataAccess.Entities;
using FirstAppApi.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace FirstAppApi.DataAccess.Repositories
{
    public class UserEFRepository : BaseEFRepository<UserEntity>, IUserRepository
    {
        public UserEFRepository(ApplicationDbContext context) : base(context)
        {
        }

        #region Methods

        public async Task<UserEntity> GetUserByEmail(string email)
        {
            UserEntity findUser = await _dbSet.FirstOrDefaultAsync(x => x.Email == email);

            return findUser;
        }

        public async Task<int> GetUserIdByEmail(string email)
        {
            UserEntity findUser = await _dbSet.FirstOrDefaultAsync(x => x.Email == email);

            int userId = findUser.Id;

            return userId;
        }

        public async Task<bool> CheckEmailInDB(string email)
        {
            bool isEmail = await _dbSet.AnyAsync(x => x.Email == email);

            return isEmail;
        }

        #endregion Methods
    }
}
