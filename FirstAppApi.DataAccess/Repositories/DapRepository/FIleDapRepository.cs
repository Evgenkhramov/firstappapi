﻿using Dapper;
using FirstAppApi.DataAccess.Entities;
using FirstAppApi.DataAccess.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FirstAppApi.DataAccess.Repositories.DapRepository
{
    public class FileDapRepository : BaseDapRepository<FileListEntity>, IFileRepository
    {
        #region Constructors

        public FileDapRepository(IConfiguration configuration) : base(configuration)
        {
        }

        #endregion Constructors

        #region Methods

        public async Task<List<FileListEntity>> GetListByTaskId(int taskId)
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT * FROM FileListEntitys WHERE TaskId = @taskId";

                var element = await dbConnection.QueryAsync<FileListEntity>(sqlQuery, new { taskId });

                List<FileListEntity> list = element.ToList<FileListEntity>();

                return list;
            }
        }

        public async Task<bool> DeleteFiles(int taskId)
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                var sqlQuery = "DELETE * FROM FileListEntitys WHERE TaskId = @taskId";

                int result = await dbConnection.ExecuteAsync(sqlQuery, new { taskId });

                return result != default(int);
            }
        }

        #endregion Methods
    }
}
