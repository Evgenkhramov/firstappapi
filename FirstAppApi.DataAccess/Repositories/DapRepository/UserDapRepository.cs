﻿using Dapper;
using FirstAppApi.DataAccess.Entities;
using FirstAppApi.DataAccess.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FirstAppApi.DataAccess.Repositories.DapRepository
{
    public class UserDapRepository : BaseDapRepository<UserEntity>, IUserRepository
    {
        #region Constructors

        public UserDapRepository(IConfiguration configuration) : base(configuration)
        {

        }

        #endregion Constructors

        #region Methods

        public async Task<UserEntity> GetUserByEmail(string email)
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT * FROM UserEntitys WHERE Email = @email";

                var element = await dbConnection.QueryAsync<UserEntity>(sqlQuery, new { email });

                UserEntity user = element.ToList().FirstOrDefault();

                return user;
            }
        }

        public async Task<int> GetUserIdByEmail(string email)
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT * FROM UserEntitys WHERE Email = @email";

                var element = await dbConnection.QueryAsync<UserEntity>(sqlQuery, new { email });

                int userId = element.ToList().FirstOrDefault().Id;

                return userId;
            }
        }

        public async Task<bool> CheckEmailInDB(string email)
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT * FROM UserEntitys WHERE Email = @email";

                var element = await dbConnection.QueryAsync<UserEntity>(sqlQuery, new { email });

                UserEntity user = element.FirstOrDefault();

                if (user != null)
                {
                    return true;
                }

                return false;
            }
        }

        #endregion Methods
    }
}
