﻿using Dapper;
using FirstAppApi.DataAccess.Entities;
using FirstAppApi.DataAccess.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FirstAppApi.DataAccess.Repositories.DapRepository
{
    public class TaskDapRepository : BaseDapRepository<TaskEntity>, ITaskRepository
    {
        #region Constructors

        public TaskDapRepository(IConfiguration configuration) : base(configuration)
        {
        }

        #endregion Constructors

        #region Methods
        public async Task<List<TaskEntity>> GetUserTasks(int userId)
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT * FROM TaskEntitys WHERE UserId = @userId";

                var element = await dbConnection.QueryAsync<TaskEntity>(sqlQuery, new { userId });

                List<TaskEntity> list = element.ToList();

                return list;
            }
        }
        #endregion Methods
    }
}
