﻿using Dapper;
using Dapper.Contrib.Extensions;
using FirstAppApi.DataAccess.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FirstAppApi.DataAccess.Repositories.DapRepository
{
    public class BaseDapRepository<T> : IBaseRepository<T> where T : class, IBaseEntity
    {
        #region Variables

        protected readonly string _connectionString;

        #endregion Variables

        #region Constructors

        public BaseDapRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        #endregion Constructors

        #region Methods

        public async Task<T> FindById(int id)
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT FROM @T WHERE Id = @id";

                IEnumerable<T> elementList = await dbConnection.QueryAsync<T>(sqlQuery, new { id, T = $"{nameof(T)}s" });

                T element = elementList.FirstOrDefault();

                return element;
            }
        }

        public async Task<List<T>> GetAll()
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                IEnumerable<T> elements = await dbConnection.GetAllAsync<T>();

                List<T> list = elements.ToList<T>();

                return list;
            }
        }

        public async Task<int> GetCount()
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT count(*) FROM @T";

                IEnumerable<T> countElements = await dbConnection.QueryAsync<T>(sqlQuery, new { T = $"{nameof(T)}s" });

                int count = countElements.Count<T>();

                return count;
            }
        }

        public async Task<bool> Insert(T entity)
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                int taskResult = await dbConnection.InsertAsync(entity);

                return taskResult != default(int);
            }
        }

        public async Task<bool> InsertRange(List<T> list)
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                int taskResult = await dbConnection.InsertAsync(list);

                return taskResult == list.Count;
            }
        }

        public async Task<bool> Update(T entity)
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                bool taskResult = await dbConnection.UpdateAsync<T>(entity);

                return taskResult;
            }
        }

        public async Task<bool> UpdateRange(List<T> list)
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                bool taskResult = await dbConnection.UpdateAsync(list);

                return taskResult;
            }
        }

        public async Task<bool> Delete(T entity)
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                bool result = await dbConnection.DeleteAsync<T>(entity);

                return result;
            }
        }

        public async Task<bool> Delete(int itemId)
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                var sqlQuery = "DELETE FROM @T WHERE Id = @itemId";

                int result = await dbConnection.ExecuteAsync(sqlQuery, new { itemId, T = $"{nameof(T)}s" });

                return result != default(int);
            }
        }

        public async Task<bool> DeleteRange(List<T> list)
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                bool result = await dbConnection.DeleteAsync(list);

                return result;
            }
        }
        public void Dispose()
        {
        }

        #endregion Methods
    }
}
