﻿using Dapper;
using FirstAppApi.DataAccess.Entities;
using FirstAppApi.DataAccess.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FirstAppApi.DataAccess.Repositories.DapRepository
{
    public class MarkersDapRepository : BaseDapRepository<MapMarkerEntity>, IMarkersRepository
    {
        #region Constructors

        public MarkersDapRepository(IConfiguration configuration) : base(configuration)
        {
        }

        #endregion Constructors

        #region Methods

        public async Task<List<MapMarkerEntity>> GetMarkersList(int taskId)
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                var sqlQuery = "SELECT * FROM MapMarkerEntitys WHERE TaskId = @taskId";

                var element = await dbConnection.QueryAsync<MapMarkerEntity>(sqlQuery, new { taskId });

                List<MapMarkerEntity> list = element.ToList();

                return list;
            }

        }

        public async Task<bool> DeleteMarkers(int taskId)
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                var sqlQuery = "DELETE * FROM MapMarkerEntitys WHERE TaskId = @taskId";

                int answer = await dbConnection.ExecuteAsync(sqlQuery, new { taskId });

                return answer != default(int);
            }
        }

        #endregion Methods
    }
}
