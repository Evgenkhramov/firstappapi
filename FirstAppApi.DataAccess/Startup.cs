﻿using FirstAppApi.DataAccess.Interfaces;
using FirstAppApi.DataAccess.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace FirstAppApi.DataAccess
{
    public class Startup
    {
        public Startup()
        {

        }

        public static void Init(IServiceCollection services)
        {
            RegistrationRepository(services);
        }

        public static void RegistrationRepository(IServiceCollection _services)
        {
            //_services.AddScoped<IFileRepository, FileDapRepository>();
            //_services.AddScoped<IMarkersRepository, MarkersDapRepository>();
            //_services.AddScoped<ITaskRepository, TaskDapRepository>();
            //_services.AddScoped<IUserRepository, UserDapRepository>();

            _services.AddScoped<IFileRepository, FileEFRepository>();
            _services.AddScoped<IMarkersRepository, MarkersEFRepository>();
            _services.AddScoped<ITaskRepository, TaskEFRepository>();
            _services.AddScoped<IUserRepository, UserEFRepository>();
        }
    }
}
