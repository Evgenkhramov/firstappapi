﻿using FirstAppApi.DataAccess.Entities;
using FirstAppApi.Models.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FirstAppApi.DataAccess
{
    public class ApplicationDbContext : IdentityDbContext<AppUser>
    {
        DbSet<MapMarkerEntity> MapMarkerEntitys { get; set; }
        DbSet<TaskEntity> TaskEntitys { get; set; }
        DbSet<UserEntity> UserEntitys { get; set; }
        DbSet<FileListEntity> FileListEntitys { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
           : base(options)
        {

        }
    }
}
