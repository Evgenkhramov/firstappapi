﻿namespace FirstAppApi.DataAccess.Entities
{
    public enum LoginType
    {
        None = 0,
        App = 1,
        Google = 2,
        Facebook = 3,
    }
}
