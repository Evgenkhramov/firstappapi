﻿namespace FirstAppApi.DataAccess.Entities
{
    public class UserEntity : BaseEntity
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Photo { get; set; }
        public string PhotoURL { get; set; }
        public LoginType TypeUserLogin { get; set; }
    }
}
