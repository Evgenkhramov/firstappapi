﻿using FirstAppApi.DataAccess.Interfaces;
using System;

namespace FirstAppApi.DataAccess.Entities
{
    public class BaseEntity : IBaseEntity
    {
        public int Id { get; set; }

        public DateTime CreateTime { get; set; }
    }
}

