﻿namespace FirstAppApi.DataAccess.Entities
{
    public class FileListEntity : BaseEntity
    {
        public int TaskId { get; set; }
        public string FileName { get; set; }
    }
}
